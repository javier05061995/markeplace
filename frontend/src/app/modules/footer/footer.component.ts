import { Component, OnInit } from '@angular/core';
import { Path } from 'src/app/config';
import { CategoriesService } from 'src/app/services/categories.service';
import { SubCategoriesService } from 'src/app/services/sub-categories.service';

declare var jQuery: any;
declare var $: any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent implements OnInit {
  path: string = Path.url;
  categories: any = [];
  render: boolean = true;
  arrayCategory: Array<any> = [];
  constructor(
    private categoriesService: CategoriesService,
    private subCategoriesService: SubCategoriesService
  ) {}

  ngOnInit(): void {
    this.categoriesService.getData().subscribe((resp: any) => {
      this.categories = resp;

      let i;
      for (i in resp) {
        this.arrayCategory.push([resp[i].name]);
      }
    });
  }
  calback() {
    if (this.render) {
      this.render = false;
      let arraySubCategories: any = [];

      this.arrayCategory.forEach((resp: string) => {
        this.subCategoriesService
          .getFilterData('category', resp)
          .subscribe((respuesta: any) => {
            let f;
            for (f in respuesta) {
              arraySubCategories.push({
                category: respuesta[f].category,
                subCategory: respuesta[f].name,
                url: respuesta[f].url,
              });
            }
            for (f in arraySubCategories) {
              if (resp == arraySubCategories[f].category) {
                $(`[category-footer = '${resp}']`).after(
                  `<a href="${arraySubCategories[f].url}">${arraySubCategories[f].subCategory}</a>`
                );
              }
            }
          });
      });
    }
  }
}
