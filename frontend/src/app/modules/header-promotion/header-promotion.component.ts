import { Component, OnInit } from '@angular/core';
import { Path } from '../../config';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-header-promotion',
  templateUrl: './header-promotion.component.html',
  styleUrls: ['./header-promotion.component.css'],
})
export class HeaderPromotionComponent implements OnInit {
  path: String = Path.url;
  top_banner: any = {};
  imagen: string = '';
  category: object = {};
  url: string = '';
  preload: boolean = false;
  constructor(private productsService: ProductsService) {}
  ngOnInit(): void {
    this.preload = true;
    this.productsService.getData().subscribe((resp: any) => {
      let index = Math.floor(Math.random() * Object.keys(resp).length);
      this.top_banner = JSON.parse(resp[Object.keys(resp)[index]].top_banner);
      this.category = resp[Object.keys(resp)[index]].category;
      this.url = resp[Object.keys(resp)[index]].url;
      this.imagen = `${this.path}img/products/${this.category}/top/${this.top_banner['IMG tag']}`;
      this.preload = false;
    });
  }
}
