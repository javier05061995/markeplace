import { Component, OnInit } from '@angular/core';
import { Path } from '../../config';
import { CategoriesService } from 'src/app/services/categories.service';
import { SubCategoriesService } from 'src/app/services/sub-categories.service';

declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  path: String = Path.url;
  categories: any = [];
  arrayTitleList: Array<any> = [];
  render: boolean = true;

  constructor(
    private categoriesService: CategoriesService,
    private subCategoriesService: SubCategoriesService
  ) {}
  ngOnInit(): void {
    this.categoriesService.getData().subscribe((resp: any) => {
      this.categories = resp;
      let i;
      for (i in resp) {
        this.arrayTitleList.push(JSON.parse(resp[i].title_list));
      }
    });
  }
  calback() {
    if (this.render) {
      let arraySubCategories: any = [];
      this.arrayTitleList.forEach((titleList) => {
        for (let i = 0; i < titleList.length; i++) {
          //tomamos la coleciond e la sub categorias
          this.subCategoriesService
            .getFilterData('title_list', titleList[i])
            .subscribe((resp) => {
              arraySubCategories.push(resp);
              let f;
              let g;
              let arrayTitleName: any = [];
              for (f in arraySubCategories) {
                for (g in arraySubCategories[f]) {
                  arrayTitleName.push({
                    titleList: arraySubCategories[f][g].title_list,
                    subCategory: arraySubCategories[f][g].name,
                    url: arraySubCategories[f][g].url,
                  });
                }
              }

              for (f in arrayTitleName) {
                if (titleList[i] == arrayTitleName[f].titleList) {
                  $(`[titleList = '${titleList[i]}']`).append(
                    `<li>
                      <a href="${arrayTitleName[f].url}">${arrayTitleName[f].subCategory}</a>
                    </li>`
                  );
                }
              }
            });
        }
      });
      this.render = false;
    }
  }
}
