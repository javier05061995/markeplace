import { Component, OnInit } from '@angular/core';

import { Path } from 'src/app/config';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-home-banner',
  templateUrl: './home-banner.component.html',
  styleUrls: ['./home-banner.component.css'],
  providers: [NgbCarouselConfig], // add NgbCarouselConfig to the component providers
})
export class HomeBannerComponent implements OnInit {
  path: string = Path.url;
  banner_home: Array<any> = [];
  category: Array<any> = [];
  url: Array<any> = [];
  preload: boolean = false;

  constructor(
    config: NgbCarouselConfig,
    private productsService: ProductsService
  ) {
    //
    config.interval = 2000;
  }
  ngOnInit(): void {
    this.preload = true;
    this.productsService.getData().subscribe((resp) => {
      let tamañoObjeto = Object.keys(resp).length;
      let index = 0;
      if (tamañoObjeto > 5) {
        index = Math.floor(Math.random() * (Object.keys(resp).length - 5));
      }

      this.productsService
        .getFilterData(Object.keys(resp)[index], 5)
        .subscribe((resp: any) => {
          let i;
          for (i in resp) {
            this.banner_home.push(JSON.parse(resp[i].horizontal_slider));
            this.category.push(resp[i].category);
            this.url.push(resp[i].url);
          }
          console.log(this.banner_home);
          this.preload = false;
        });
    });
  }
}
