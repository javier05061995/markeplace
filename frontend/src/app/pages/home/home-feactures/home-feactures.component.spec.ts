import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeFeacturesComponent } from './home-feactures.component';

describe('HomeFeacturesComponent', () => {
  let component: HomeFeacturesComponent;
  let fixture: ComponentFixture<HomeFeacturesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomeFeacturesComponent]
    });
    fixture = TestBed.createComponent(HomeFeacturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
