import { Component, OnInit } from '@angular/core';
import { Path } from 'src/app/config';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-home-promotions',
  templateUrl: './home-promotions.component.html',
  styleUrls: ['./home-promotions.component.css'],
})
export class HomePromotionsComponent implements OnInit {
  path: string = Path.url;
  banner_default: Array<any> = [];
  category: Array<any> = [];
  url: Array<any> = [];
  preload: boolean = false;

  constructor(private productsService: ProductsService) {}

  ngOnInit(): void {
    this.preload = true;
    this.productsService.getData().subscribe((resp) => {
      let tamañoObjeto = Object.keys(resp).length;
      let index = 0;
      if (tamañoObjeto > 5) {
        index = Math.floor(Math.random() * (Object.keys(resp).length - 5));
      }

      this.productsService
        .getFilterData(Object.keys(resp)[index], 3)
        .subscribe((resp: any) => {
          let i;
          for (i in resp) {
            this.banner_default.push(resp[i].default_banner);
            this.category.push(resp[i].category);
            this.url.push(resp[i].url);
          }
          console.log(this.banner_default);
          this.preload = false;
        });
    });
  }
}
